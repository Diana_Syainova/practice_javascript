function runButtonClick(){
	var x1StrValue = document.getElementById('x1').value;
	var x2StrValue = document.getElementById('x2').value;
	var x1 = parseInt(x1StrValue);
	var x2 = parseInt(x2StrValue);
	var resultDiv = document.getElementById('result');
	
	resultDiv.textContent = "";

	if (x1StrValue === "" || x2StrValue === ""){
		alert("Поля x1 и x2 должны быть заполнены.");
	}
	else if(Number.isNaN(x1) || Number.isNaN(x2)){
		alert("В поля x1 и x2 должны быть введены числовые значения.");
	}
	else if ( x1 >= x2){
		alert("Значение x1 должно быть больше значения x2.");
	}
	else{
		var operation = document.getElementsByName("operations");
		if (operation[0].checked) {
			var sum = 0;
			for (var i = x1; i <= x2; i++){
				sum += i;
			}
			resultDiv.append(sum);
		}
		else if (operation[1].checked){
			var mult = 1;
			for (var i = x1; i <= x2; i++){
				mult *= i;
			}
			resultDiv.append(mult);			
		}
		else if (operation[2].checked){
			var simpleNumrer = false;
			Outer:
				for (var i = Math.max(x1,2); i <= x2; i++){
					for (var j = 2;  j < i; j++){
						if (i % j == 0) continue Outer;	
					}
					simpleNumrer = true;
					resultDiv.append(i+" ");
				}	
			if (simpleNumrer == false){
				alert("Простых чисел в данном диапазоне нет.");
			}
		}				
		else {
			alert("Укажите операцию.");
		}
	}
}
function clearButtonClick(){
	document.getElementById('x1').value="";
	document.getElementById('x2').value="";
	document.getElementById('result').textContent="";
	document.getElementsByName("operations")[0].checked = false;
	document.getElementsByName("operations")[1].checked = false;
	document.getElementsByName("operations")[2].checked = false;
}